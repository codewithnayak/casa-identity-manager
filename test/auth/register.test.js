const Register = require("../../controllers/auth/Register");
const { expect } = require("chai");
const httpMock = require("node-mocks-http");
const sinon = require("sinon");
const User = require("../../db/models/user");
const Notification = require("../../services/notification");

describe("Regsiter user functionality test", () => {
  let req = {};
  let res = {};
  beforeEach(() => {
    req = httpMock.createRequest();
    res = httpMock.createResponse();
  });

  describe("Model validation test ", () => {
    it("Should return bad request if email is not valid ", async function () {
      req.body = {
        email: "notvalidemail",
        password: "does not matter ",
      };

      const response = await Register(req, res);
      const data = response._getJSONData();
      expect(data).to.be.ok;
      expect(data).to.include("must be a valid email");
      expect(response.statusCode).equal(400);
    });
    it("Should return bad request if passsword not provided ", async function () {
      req.body = {
        email: "someone@example.com",
        password: "",
      };

      const response = await Register(req, res);
      const data = response._getJSONData();
      expect(data).to.be.ok;
      expect(data).to.include("password");
      expect(response.statusCode).equal(400);
    });
  });

  describe("Database save and notification test ", () => {
    let userModelStub = {};
    let verifyEmailStub = {};
    beforeEach(() => {
      userModelStub = sinon.stub(User, "create");
      verifyEmailStub = sinon.stub(Notification, "SendVerifyEmailNotification");
    });
    afterEach(() => {
      userModelStub.restore();
      verifyEmailStub.restore();
    });

    it("Should save the record in db and trigger verify email notification  ", async function () {
      //ARRANGE
      req.body = {
        email: "someone@example.com",
        password: "strongpassword",
      };

      userModelStub.returns({
        userId: "userId",
        email: "someone@example.com",
      });

      verifyEmailStub.returns(true);
      //ACT
      const response = await Register(req, res);
      const data = response._getJSONData();

      //ASSERT
      userModelStub.calledOnce;
      verifyEmailStub.calledOnce;
      expect(data).to.be.ok;
      expect(data.userId).not.to.be.empty;
      expect(data.message).to.include("New user created successfully");
      expect(response.statusCode).equal(201);
    });
    it("Should return error if notification service fails ", async function () {
      //ARRANGE
      req.body = {
        email: "someone@example.com",
        password: "strongpassword",
      };

      userModelStub.returns({
        userId: "userId",
        email: "someone@example.com",
      });

      verifyEmailStub.returns(false);
      //ACT
      const response = await Register(req, res);
      //ASSERT
      userModelStub.calledOnce;
      verifyEmailStub.calledOnce;
      expect(response.statusCode).equal(500);
    });
  });
});
