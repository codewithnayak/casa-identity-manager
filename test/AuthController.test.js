const { expect } = require("chai");
const httpMock = require("node-mocks-http");
const { ForgotPassword } = require("../controllers/auth");
const sinon = require("sinon");
const User = require("../db/models/user");

describe("Forgot password functionality test ", () => {
  describe("Request body validation tests  ", () => {
    let req = {};
    let res = {};
    let stub;
    beforeEach(() => {
      req = httpMock.createRequest();
      res = httpMock.createResponse();
      stub = sinon.stub(User, "findOne");
    });
    afterEach(() => {
      stub.restore();
    });
    it("If req is not a valid email should return bad request ", async function () {
      req.body = {
        email: "somesome",
      };
      const response = await ForgotPassword(req, res);
      const data = response._getJSONData();
      expect(response.statusCode).equal(400);
      expect(data.error).contains("email");
    });
    it("If no request body present  should return bad request ", async function () {
      const response = await ForgotPassword(req, res);
      const data = response._getJSONData();
      expect(response.statusCode).equal(400);
      expect(data.error).contains("required");
    });
    it("If user does not exist should return not found  ", async function () {
      stub.returns(null);
      req.body = {
        email: "someone@example.com",
      };
      const response = await ForgotPassword(req, res);
      const data = response._getJSONData();
      expect(response.statusCode).equal(400);
      expect(data.message).equal("User not found");
      stub.calledOnce;
      stub.calledOnceWithExactly(req.body);
    });
    it("If user exists should return success with status no content", async function () {
      const dbEntity = {
        ussrId: "someId",
        email: "some email",
        password: "some password ",
      };
      stub.returns(dbEntity);
      req.body = {
        email: "someone@example.com",
      };
      const response = await ForgotPassword(req, res);
      expect(response.statusCode).equal(204);
      stub.calledOnce;
      stub.calledOnceWithExactly(req.body);
    });
  });
});
