require("express");
const { v4: guid } = require("uuid");
const User = require("../../db/models/user");
const Notification = require("../../services/notification");
const Joi = require("joi");

const registerSchema = Joi.object().keys({
  email: Joi.string().required().email(),
  password: Joi.string().required(),
});

const Register = async (req, res) => {
  const { error, value } = registerSchema.validate(req.body);

  if (error) return res.status(400).json(error.message);

  const { email, password } = value;
  const user = await User.create({
    userId: guid(),
    email,
    password,
    createdAt: new Date(),
  });

  const hasEmailSent = await Notification.SendVerifyEmailNotification(email);
  if (!hasEmailSent) {
    return res.status(500).send("An unexpected error occurred ");
  }
  return res.status(201).json({
    success: true,
    userId: user.userId,
    message: "New user created successfully",
  });
};

module.exports = Register;
