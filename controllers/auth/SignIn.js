require("express");
const { v4: guid } = require("uuid");
const User = require("../../db/models/user");
const { VerifyEmail } = require("../../services/notification");
const Joi = require("joi");
module.exports = SignIn = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email: email });
  if (user) {
    return res.json({
      success: true,
      token: "jwt token ",
    });
  }
  return res.status(404).json({
    success: false,
    error: "email or password is wrong ",
  });
};
