const Register = require("./Register");
const ForgotPassword = require("./ForgotPassword");
const Profile = require("./Profile");
const ResetPassword = require("./ResetPassword");
const SignIn = require("./SignIn");

/*
 * Holds all the identity related functionalities
 * SignIn , SignUp , ResetPassword , ForgotPassword ,
 * Profile and verify email
 */

module.exports = {
  Register,
  ForgotPassword,
  Profile,
  ResetPassword,
  SignIn,
};
