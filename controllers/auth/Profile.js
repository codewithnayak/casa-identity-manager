require("express");
const { v4: guid } = require("uuid");
const User = require("../../db/models/user");
const { VerifyEmail } = require("../../services/notification");
const Joi = require("joi");

const Profile = async (req, res) => {
  const { email } = req.body;
  const user = await User.findOne({ email: email });

  if (user) {
    return res.status(200).json(user);
  }

  return res.status(404).json("user does not exists");
};

module.exports = Profile;
