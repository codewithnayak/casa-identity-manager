require("express");
const { v4: guid } = require("uuid");
const User = require("../../db/models/user");
const { VerifyEmail } = require("../../services/notification");
const Joi = require("joi");

const getId = (secret) => {
  return secret;
};

module.exports = ResetPassword = async (req, res) => {
  const { secret, password } = req.body;
  const userId = getId(secret);
  const user = await User.findOne({ userId });
  if (!user) return res.status(404).json({ message: "user does not exist " });
  await User.updateOne({ userId }, { password });
  return res.status(204).send();
};
