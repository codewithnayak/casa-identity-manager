require("express");
const { v4: guid } = require("uuid");
const User = require("../../db/models/user");
const { VerifyEmail } = require("../../services/notification");
const Joi = require("joi");

const ForgotPassword = async (req, res) => {
  const { email } = req.body;
  const isValid = Joi.string().required().email().validate(email);

  if (isValid.error) {
    const { message } = isValid.error;
    return res.status(400).json({
      error: message,
    });
  }

  const user = await User.findOne({ email });
  if (!user) return res.status(400).json({ message: "User not found" });
  return res.status(204).send();
};

module.exports = ForgotPassword;
