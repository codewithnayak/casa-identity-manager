const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  userId: String,
  email: String,
  password: String,
  createdAt: Date,
});

const User = new mongoose.model("User", userSchema);

module.exports = User;
