const mongoose = require("mongoose");

const connect = (connStr) => {
  mongoose.connect(connStr, { useNewUrlParser: true });
  const db = mongoose.connection;
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", function () {
    console.log(
      `Successfully connected to mongodb - IdentityStore on ${connStr}`
    );
  });
};

module.exports = {
  connect,
};
