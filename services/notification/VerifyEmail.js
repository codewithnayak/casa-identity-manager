const Joi = require("joi");
const axios = require("axios");
const logger = require("../../utility/logger/logger");
const StandardTemplateName = "VERIFY_EMAIL";

/*
 * @param {string} => email
 */
module.exports = SendVerifyEmailNotification = async (email) => {
  const { error, value } = Joi.string().required().email().validate(email);
  const baseUrl = process.env.NOTIFICATION_SERVICE_BASE_URL;
  if (error) {
    logger.error("input is not a valid email ");
    throw new Error("Argument should be a valid email ");
  }
  const data = {
    value,
    template: StandardTemplateName,
    htmlBody: true,
  };
  const { status, statusText } = await axios.post(`${baseUrl}/email`, data);
  if (status >= 200 && status <= 299) {
    logger.info(
      `email verification notification successfully sent for email ${email}`
    );
    return true;
  }
  logger.error(
    `Exception occurred while sending notification error: ${statusText} `
  );
  return false;
};
