const express = require("express");
const router = express.Router();
const cors = require("cors");
const {
  SignIn,
  Register,
  Profile,
  ForgotPassword,
  ResetPassword,
} = require("../controllers/auth");

const corsOptions = {
  origin: process.env.ALLOWED_ORIGIN,
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

router.post("/signin", cors(corsOptions), SignIn);

router.post("/signup", cors(corsOptions), Register);

router.post("/profile", cors(corsOptions), Profile);

router.post("/forgot-password", cors(corsOptions), ForgotPassword);

router.post("/reset-password", cors(corsOptions), ResetPassword);

module.exports = router;
